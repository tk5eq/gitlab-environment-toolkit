---
- name: Setup GitLab config file
  template:
    src: templates/consul.gitlab.rb.j2
    dest: /etc/gitlab/gitlab.rb
  tags: reconfigure

- name: Check if custom config exists
  stat:
    path: "{{ consul_custom_config_file }}"
  delegate_to: localhost
  become: false
  tags: reconfigure
  register: consul_custom_config_file_path

- name: Setup custom GitLab config file
  template:
    src: "{{ consul_custom_config_file }}"
    dest: "/etc/gitlab/gitlab.consul.custom.rb"
    mode: 0644
  tags: reconfigure
  when: consul_custom_config_file_path.stat.exists

- name: Propagate Secrets if existing
  include_role:
    name: common
    tasks_from: secrets
  tags:
    - reconfigure
    - secrets

- name: Stop Consul
  shell: gitlab-ctl stop consul
  tags:
    - reconfigure

- name: Clear old Consul data
  file:
    path: /var/opt/gitlab/consul/data
    state: absent
  tags:
    - reconfigure

- name: Reconfigure Consul
  command: gitlab-ctl reconfigure
  register: result
  retries: 3
  until: result is success
  tags: reconfigure

- name: Propagate Secrets if new
  include_role:
    name: common
    tasks_from: secrets
  vars:
    gitlab_secrets_reconfigure: true
  tags:
    - reconfigure
    - secrets

- name: Restart Consul
  command: gitlab-ctl restart
  register: result
  retries: 2
  until: result is success
  tags:
    - reconfigure
    - restart

- name: Create skip-auto-reconfigure file
  file:
    path: /etc/gitlab/skip-auto-reconfigure
    state: touch
    mode: u=rw,g=r,o=r

- name: Run Custom Tasks
  block:
    - name: Check if Custom Tasks file exists
      stat:
        path: "{{ consul_custom_tasks_file }}"
      register: consul_custom_tasks_file_path
      delegate_to: localhost
      become: false

    - name: Run Custom Tasks
      include_tasks:
        file: "{{ consul_custom_tasks_file }}"
        apply:
          tags: custom_tasks
      when: consul_custom_tasks_file_path.stat.exists
  tags: custom_tasks
